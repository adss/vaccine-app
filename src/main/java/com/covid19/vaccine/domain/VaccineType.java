package com.covid19.vaccine.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A VaccineType.
 */
@Entity
@Table(name = "vaccine_type")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class VaccineType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 3, max = 20)
    @Column(name = "name", length = 20, nullable = false, unique = true)
    private String name;

    
    @Lob
    @Column(name = "type_image")
    private byte[] typeImage;

    @Column(name = "type_image_content_type")
    private String typeImageContentType;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public VaccineType name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getTypeImage() {
        return typeImage;
    }

    public VaccineType typeImage(byte[] typeImage) {
        this.typeImage = typeImage;
        return this;
    }

    public void setTypeImage(byte[] typeImage) {
        this.typeImage = typeImage;
    }

    public String getTypeImageContentType() {
        return typeImageContentType;
    }

    public VaccineType typeImageContentType(String typeImageContentType) {
        this.typeImageContentType = typeImageContentType;
        return this;
    }

    public void setTypeImageContentType(String typeImageContentType) {
        this.typeImageContentType = typeImageContentType;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VaccineType)) {
            return false;
        }
        return id != null && id.equals(((VaccineType) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VaccineType{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", typeImage='" + getTypeImage() + "'" +
            ", typeImageContentType='" + getTypeImageContentType() + "'" +
            "}";
    }
}
