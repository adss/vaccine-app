package com.covid19.vaccine.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * A Vaccine.
 */
@Entity
@Table(name = "vaccine")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Vaccine implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 3, max = 20)
    @Column(name = "name", length = 20, nullable = false)
    private String name;

    @Lob
    @Column(name = "company")
    private byte[] company;

    @Column(name = "company_content_type")
    private String companyContentType;

    @Lob
    @Column(name = "company_2")
    private byte[] company2;

    @Column(name = "company_2_content_type")
    private String company2ContentType;

    @Column(name = "efficacy", precision = 21, scale = 2)
    private BigDecimal efficacy;

    @Column(name = "dose")
    private String dose;

    @Column(name = "type")
    private String type;

    @Column(name = "storage")
    private String storage;

    
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "description", nullable = false)
    private String description;

    @OneToOne
    @JoinColumn(unique = true)
    private VaccineType vaccineType;

    @OneToMany(mappedBy = "vaccine")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Phase> phases = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(name = "vaccine_countries",
               joinColumns = @JoinColumn(name = "vaccine_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "countries_id", referencedColumnName = "id"))
    private Set<Country> countries = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Vaccine name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getCompany() {
        return company;
    }

    public Vaccine company(byte[] company) {
        this.company = company;
        return this;
    }

    public void setCompany(byte[] company) {
        this.company = company;
    }

    public String getCompanyContentType() {
        return companyContentType;
    }

    public Vaccine companyContentType(String companyContentType) {
        this.companyContentType = companyContentType;
        return this;
    }

    public void setCompanyContentType(String companyContentType) {
        this.companyContentType = companyContentType;
    }

    public byte[] getCompany2() {
        return company2;
    }

    public Vaccine company2(byte[] company2) {
        this.company2 = company2;
        return this;
    }

    public void setCompany2(byte[] company2) {
        this.company2 = company2;
    }

    public String getCompany2ContentType() {
        return company2ContentType;
    }

    public Vaccine company2ContentType(String company2ContentType) {
        this.company2ContentType = company2ContentType;
        return this;
    }

    public void setCompany2ContentType(String company2ContentType) {
        this.company2ContentType = company2ContentType;
    }

    public BigDecimal getEfficacy() {
        return efficacy;
    }

    public Vaccine efficacy(BigDecimal efficacy) {
        this.efficacy = efficacy;
        return this;
    }

    public void setEfficacy(BigDecimal efficacy) {
        this.efficacy = efficacy;
    }

    public String getDose() {
        return dose;
    }

    public Vaccine dose(String dose) {
        this.dose = dose;
        return this;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getType() {
        return type;
    }

    public Vaccine type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStorage() {
        return storage;
    }

    public Vaccine storage(String storage) {
        this.storage = storage;
        return this;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public String getDescription() {
        return description;
    }

    public Vaccine description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public VaccineType getVaccineType() {
        return vaccineType;
    }

    public Vaccine vaccineType(VaccineType vaccineType) {
        this.vaccineType = vaccineType;
        return this;
    }

    public void setVaccineType(VaccineType vaccineType) {
        this.vaccineType = vaccineType;
    }

    public Set<Phase> getPhases() {
        return phases;
    }

    public Vaccine phases(Set<Phase> phases) {
        this.phases = phases;
        return this;
    }

    public Vaccine addPhases(Phase phase) {
        this.phases.add(phase);
        phase.setVaccine(this);
        return this;
    }

    public Vaccine removePhases(Phase phase) {
        this.phases.remove(phase);
        phase.setVaccine(null);
        return this;
    }

    public void setPhases(Set<Phase> phases) {
        this.phases = phases;
    }

    public Set<Country> getCountries() {
        return countries;
    }

    public Vaccine countries(Set<Country> countries) {
        this.countries = countries;
        return this;
    }

    public Vaccine addCountries(Country country) {
        this.countries.add(country);
        country.getVaccines().add(this);
        return this;
    }

    public Vaccine removeCountries(Country country) {
        this.countries.remove(country);
        country.getVaccines().remove(this);
        return this;
    }

    public void setCountries(Set<Country> countries) {
        this.countries = countries;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Vaccine)) {
            return false;
        }
        return id != null && id.equals(((Vaccine) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Vaccine{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", company='" + getCompany() + "'" +
            ", companyContentType='" + getCompanyContentType() + "'" +
            ", company2='" + getCompany2() + "'" +
            ", company2ContentType='" + getCompany2ContentType() + "'" +
            ", efficacy=" + getEfficacy() +
            ", dose='" + getDose() + "'" +
            ", type='" + getType() + "'" +
            ", storage='" + getStorage() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
