/**
 * View Models used by Spring MVC REST controllers.
 */
package com.covid19.vaccine.web.rest.vm;
