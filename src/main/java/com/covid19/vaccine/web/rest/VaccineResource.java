package com.covid19.vaccine.web.rest;

import com.covid19.vaccine.service.VaccineService;
import com.covid19.vaccine.web.rest.errors.BadRequestAlertException;
import com.covid19.vaccine.service.dto.VaccineDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.covid19.vaccine.domain.Vaccine}.
 */
@RestController
@RequestMapping("/api")
public class VaccineResource {

    private final Logger log = LoggerFactory.getLogger(VaccineResource.class);

    private static final String ENTITY_NAME = "vaccineAppVaccine";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VaccineService vaccineService;

    public VaccineResource(VaccineService vaccineService) {
        this.vaccineService = vaccineService;
    }

    /**
     * {@code POST  /vaccines} : Create a new vaccine.
     *
     * @param vaccineDTO the vaccineDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new vaccineDTO, or with status {@code 400 (Bad Request)} if the vaccine has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/vaccines")
    public ResponseEntity<VaccineDTO> createVaccine(@Valid @RequestBody VaccineDTO vaccineDTO) throws URISyntaxException {
        log.debug("REST request to save Vaccine : {}", vaccineDTO);
        if (vaccineDTO.getId() != null) {
            throw new BadRequestAlertException("A new vaccine cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VaccineDTO result = vaccineService.save(vaccineDTO);
        return ResponseEntity.created(new URI("/api/vaccines/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /vaccines} : Updates an existing vaccine.
     *
     * @param vaccineDTO the vaccineDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated vaccineDTO,
     * or with status {@code 400 (Bad Request)} if the vaccineDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the vaccineDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/vaccines")
    public ResponseEntity<VaccineDTO> updateVaccine(@Valid @RequestBody VaccineDTO vaccineDTO) throws URISyntaxException {
        log.debug("REST request to update Vaccine : {}", vaccineDTO);
        if (vaccineDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        VaccineDTO result = vaccineService.save(vaccineDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, vaccineDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /vaccines} : get all the vaccines.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of vaccines in body.
     */
    @GetMapping("/vaccines")
    public ResponseEntity<List<VaccineDTO>> getAllVaccines(Pageable pageable, @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of Vaccines");
        Page<VaccineDTO> page;
        if (eagerload) {
            page = vaccineService.findAllWithEagerRelationships(pageable);
        } else {
            page = vaccineService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /vaccines/:id} : get the "id" vaccine.
     *
     * @param id the id of the vaccineDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the vaccineDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/vaccines/{id}")
    public ResponseEntity<VaccineDTO> getVaccine(@PathVariable Long id) {
        log.debug("REST request to get Vaccine : {}", id);
        Optional<VaccineDTO> vaccineDTO = vaccineService.findOne(id);
        return ResponseUtil.wrapOrNotFound(vaccineDTO);
    }

    /**
     * {@code DELETE  /vaccines/:id} : delete the "id" vaccine.
     *
     * @param id the id of the vaccineDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/vaccines/{id}")
    public ResponseEntity<Void> deleteVaccine(@PathVariable Long id) {
        log.debug("REST request to delete Vaccine : {}", id);
        vaccineService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
