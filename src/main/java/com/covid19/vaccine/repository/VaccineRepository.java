package com.covid19.vaccine.repository;

import com.covid19.vaccine.domain.Vaccine;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Vaccine entity.
 */
@Repository
public interface VaccineRepository extends JpaRepository<Vaccine, Long> {

    @Query(value = "select distinct vaccine from Vaccine vaccine left join fetch vaccine.countries",
        countQuery = "select count(distinct vaccine) from Vaccine vaccine")
    Page<Vaccine> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct vaccine from Vaccine vaccine left join fetch vaccine.countries")
    List<Vaccine> findAllWithEagerRelationships();

    @Query("select vaccine from Vaccine vaccine left join fetch vaccine.countries where vaccine.id =:id")
    Optional<Vaccine> findOneWithEagerRelationships(@Param("id") Long id);
}
