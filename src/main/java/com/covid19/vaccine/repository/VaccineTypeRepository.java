package com.covid19.vaccine.repository;

import com.covid19.vaccine.domain.VaccineType;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the VaccineType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VaccineTypeRepository extends JpaRepository<VaccineType, Long> {
}
