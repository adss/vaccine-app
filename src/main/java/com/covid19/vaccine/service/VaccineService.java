package com.covid19.vaccine.service;

import com.covid19.vaccine.service.dto.VaccineDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.covid19.vaccine.domain.Vaccine}.
 */
public interface VaccineService {

    /**
     * Save a vaccine.
     *
     * @param vaccineDTO the entity to save.
     * @return the persisted entity.
     */
    VaccineDTO save(VaccineDTO vaccineDTO);

    /**
     * Get all the vaccines.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<VaccineDTO> findAll(Pageable pageable);

    /**
     * Get all the vaccines with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<VaccineDTO> findAllWithEagerRelationships(Pageable pageable);


    /**
     * Get the "id" vaccine.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<VaccineDTO> findOne(Long id);

    /**
     * Delete the "id" vaccine.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
