package com.covid19.vaccine.service.impl;

import com.covid19.vaccine.service.VaccineService;
import com.covid19.vaccine.domain.Vaccine;
import com.covid19.vaccine.repository.VaccineRepository;
import com.covid19.vaccine.service.dto.VaccineDTO;
import com.covid19.vaccine.service.mapper.VaccineMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Vaccine}.
 */
@Service
@Transactional
public class VaccineServiceImpl implements VaccineService {

    private final Logger log = LoggerFactory.getLogger(VaccineServiceImpl.class);

    private final VaccineRepository vaccineRepository;

    private final VaccineMapper vaccineMapper;

    public VaccineServiceImpl(VaccineRepository vaccineRepository, VaccineMapper vaccineMapper) {
        this.vaccineRepository = vaccineRepository;
        this.vaccineMapper = vaccineMapper;
    }

    @Override
    public VaccineDTO save(VaccineDTO vaccineDTO) {
        log.debug("Request to save Vaccine : {}", vaccineDTO);
        Vaccine vaccine = vaccineMapper.toEntity(vaccineDTO);
        vaccine = vaccineRepository.save(vaccine);
        return vaccineMapper.toDto(vaccine);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<VaccineDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Vaccines");
        return vaccineRepository.findAll(pageable)
            .map(vaccineMapper::toDto);
    }


    public Page<VaccineDTO> findAllWithEagerRelationships(Pageable pageable) {
        return vaccineRepository.findAllWithEagerRelationships(pageable).map(vaccineMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<VaccineDTO> findOne(Long id) {
        log.debug("Request to get Vaccine : {}", id);
        return vaccineRepository.findOneWithEagerRelationships(id)
            .map(vaccineMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Vaccine : {}", id);
        vaccineRepository.deleteById(id);
    }
}
