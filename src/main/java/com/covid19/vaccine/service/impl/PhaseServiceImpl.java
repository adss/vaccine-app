package com.covid19.vaccine.service.impl;

import com.covid19.vaccine.service.PhaseService;
import com.covid19.vaccine.domain.Phase;
import com.covid19.vaccine.repository.PhaseRepository;
import com.covid19.vaccine.service.dto.PhaseDTO;
import com.covid19.vaccine.service.mapper.PhaseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Phase}.
 */
@Service
@Transactional
public class PhaseServiceImpl implements PhaseService {

    private final Logger log = LoggerFactory.getLogger(PhaseServiceImpl.class);

    private final PhaseRepository phaseRepository;

    private final PhaseMapper phaseMapper;

    public PhaseServiceImpl(PhaseRepository phaseRepository, PhaseMapper phaseMapper) {
        this.phaseRepository = phaseRepository;
        this.phaseMapper = phaseMapper;
    }

    @Override
    public PhaseDTO save(PhaseDTO phaseDTO) {
        log.debug("Request to save Phase : {}", phaseDTO);
        Phase phase = phaseMapper.toEntity(phaseDTO);
        phase = phaseRepository.save(phase);
        return phaseMapper.toDto(phase);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PhaseDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Phases");
        return phaseRepository.findAll(pageable)
            .map(phaseMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<PhaseDTO> findOne(Long id) {
        log.debug("Request to get Phase : {}", id);
        return phaseRepository.findById(id)
            .map(phaseMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Phase : {}", id);
        phaseRepository.deleteById(id);
    }
}
