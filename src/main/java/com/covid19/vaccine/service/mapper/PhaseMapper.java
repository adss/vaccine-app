package com.covid19.vaccine.service.mapper;


import com.covid19.vaccine.domain.*;
import com.covid19.vaccine.service.dto.PhaseDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Phase} and its DTO {@link PhaseDTO}.
 */
@Mapper(componentModel = "spring", uses = {VaccineMapper.class})
public interface PhaseMapper extends EntityMapper<PhaseDTO, Phase> {

    @Mapping(source = "vaccine.id", target = "vaccineId")
    PhaseDTO toDto(Phase phase);

    @Mapping(source = "vaccineId", target = "vaccine")
    Phase toEntity(PhaseDTO phaseDTO);

    default Phase fromId(Long id) {
        if (id == null) {
            return null;
        }
        Phase phase = new Phase();
        phase.setId(id);
        return phase;
    }
}
