package com.covid19.vaccine.service.mapper;


import com.covid19.vaccine.domain.*;
import com.covid19.vaccine.service.dto.VaccineTypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link VaccineType} and its DTO {@link VaccineTypeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface VaccineTypeMapper extends EntityMapper<VaccineTypeDTO, VaccineType> {



    default VaccineType fromId(Long id) {
        if (id == null) {
            return null;
        }
        VaccineType vaccineType = new VaccineType();
        vaccineType.setId(id);
        return vaccineType;
    }
}
