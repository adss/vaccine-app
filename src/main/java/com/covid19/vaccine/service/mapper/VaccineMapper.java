package com.covid19.vaccine.service.mapper;


import com.covid19.vaccine.domain.*;
import com.covid19.vaccine.service.dto.VaccineDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Vaccine} and its DTO {@link VaccineDTO}.
 */
@Mapper(componentModel = "spring", uses = {VaccineTypeMapper.class, CountryMapper.class})
public interface VaccineMapper extends EntityMapper<VaccineDTO, Vaccine> {

    @Mapping(source = "vaccineType.id", target = "vaccineTypeId")
    @Mapping(source = "vaccineType.name", target = "vaccineTypeName")
    VaccineDTO toDto(Vaccine vaccine);

    @Mapping(source = "vaccineTypeId", target = "vaccineType")
    @Mapping(target = "phases", ignore = true)
    @Mapping(target = "removePhases", ignore = true)
    @Mapping(target = "removeCountries", ignore = true)
    Vaccine toEntity(VaccineDTO vaccineDTO);

    default Vaccine fromId(Long id) {
        if (id == null) {
            return null;
        }
        Vaccine vaccine = new Vaccine();
        vaccine.setId(id);
        return vaccine;
    }
}
