package com.covid19.vaccine.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.covid19.vaccine.domain.VaccineType} entity.
 */
public class VaccineTypeDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(min = 3, max = 20)
    private String name;

    
    @Lob
    private byte[] typeImage;

    private String typeImageContentType;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getTypeImage() {
        return typeImage;
    }

    public void setTypeImage(byte[] typeImage) {
        this.typeImage = typeImage;
    }

    public String getTypeImageContentType() {
        return typeImageContentType;
    }

    public void setTypeImageContentType(String typeImageContentType) {
        this.typeImageContentType = typeImageContentType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VaccineTypeDTO)) {
            return false;
        }

        return id != null && id.equals(((VaccineTypeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VaccineTypeDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", typeImage='" + getTypeImage() + "'" +
            "}";
    }
}
