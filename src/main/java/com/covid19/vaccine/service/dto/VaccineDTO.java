package com.covid19.vaccine.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.covid19.vaccine.domain.Vaccine} entity.
 */
public class VaccineDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(min = 3, max = 20)
    private String name;

    @Lob
    private byte[] company;

    private String companyContentType;
    @Lob
    private byte[] company2;

    private String company2ContentType;
    private BigDecimal efficacy;

    private String dose;

    private String type;

    private String storage;

    
    @Lob
    private String description;


    private Long vaccineTypeId;

    private String vaccineTypeName;
    private Set<CountryDTO> countries = new HashSet<>();
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getCompany() {
        return company;
    }

    public void setCompany(byte[] company) {
        this.company = company;
    }

    public String getCompanyContentType() {
        return companyContentType;
    }

    public void setCompanyContentType(String companyContentType) {
        this.companyContentType = companyContentType;
    }

    public byte[] getCompany2() {
        return company2;
    }

    public void setCompany2(byte[] company2) {
        this.company2 = company2;
    }

    public String getCompany2ContentType() {
        return company2ContentType;
    }

    public void setCompany2ContentType(String company2ContentType) {
        this.company2ContentType = company2ContentType;
    }

    public BigDecimal getEfficacy() {
        return efficacy;
    }

    public void setEfficacy(BigDecimal efficacy) {
        this.efficacy = efficacy;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getVaccineTypeId() {
        return vaccineTypeId;
    }

    public void setVaccineTypeId(Long vaccineTypeId) {
        this.vaccineTypeId = vaccineTypeId;
    }

    public String getVaccineTypeName() {
        return vaccineTypeName;
    }

    public void setVaccineTypeName(String vaccineTypeName) {
        this.vaccineTypeName = vaccineTypeName;
    }

    public Set<CountryDTO> getCountries() {
        return countries;
    }

    public void setCountries(Set<CountryDTO> countries) {
        this.countries = countries;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VaccineDTO)) {
            return false;
        }

        return id != null && id.equals(((VaccineDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VaccineDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", company='" + getCompany() + "'" +
            ", company2='" + getCompany2() + "'" +
            ", efficacy=" + getEfficacy() +
            ", dose='" + getDose() + "'" +
            ", type='" + getType() + "'" +
            ", storage='" + getStorage() + "'" +
            ", description='" + getDescription() + "'" +
            ", vaccineTypeId=" + getVaccineTypeId() +
            ", vaccineTypeName='" + getVaccineTypeName() + "'" +
            ", countries='" + getCountries() + "'" +
            "}";
    }
}
