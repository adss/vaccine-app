package com.covid19.vaccine.service;

import com.covid19.vaccine.service.dto.VaccineTypeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.covid19.vaccine.domain.VaccineType}.
 */
public interface VaccineTypeService {

    /**
     * Save a vaccineType.
     *
     * @param vaccineTypeDTO the entity to save.
     * @return the persisted entity.
     */
    VaccineTypeDTO save(VaccineTypeDTO vaccineTypeDTO);

    /**
     * Get all the vaccineTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<VaccineTypeDTO> findAll(Pageable pageable);


    /**
     * Get the "id" vaccineType.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<VaccineTypeDTO> findOne(Long id);

    /**
     * Delete the "id" vaccineType.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
