package com.covid19.vaccine.web.rest;

import com.covid19.vaccine.VaccineApp;
import com.covid19.vaccine.domain.Vaccine;
import com.covid19.vaccine.repository.VaccineRepository;
import com.covid19.vaccine.service.VaccineService;
import com.covid19.vaccine.service.dto.VaccineDTO;
import com.covid19.vaccine.service.mapper.VaccineMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link VaccineResource} REST controller.
 */
@SpringBootTest(classes = VaccineApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class VaccineResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final byte[] DEFAULT_COMPANY = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_COMPANY = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_COMPANY_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_COMPANY_CONTENT_TYPE = "image/png";

    private static final byte[] DEFAULT_COMPANY_2 = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_COMPANY_2 = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_COMPANY_2_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_COMPANY_2_CONTENT_TYPE = "image/png";

    private static final BigDecimal DEFAULT_EFFICACY = new BigDecimal(1);
    private static final BigDecimal UPDATED_EFFICACY = new BigDecimal(2);

    private static final String DEFAULT_DOSE = "AAAAAAAAAA";
    private static final String UPDATED_DOSE = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_STORAGE = "AAAAAAAAAA";
    private static final String UPDATED_STORAGE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private VaccineRepository vaccineRepository;

    @Mock
    private VaccineRepository vaccineRepositoryMock;

    @Autowired
    private VaccineMapper vaccineMapper;

    @Mock
    private VaccineService vaccineServiceMock;

    @Autowired
    private VaccineService vaccineService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restVaccineMockMvc;

    private Vaccine vaccine;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Vaccine createEntity(EntityManager em) {
        Vaccine vaccine = new Vaccine()
            .name(DEFAULT_NAME)
            .company(DEFAULT_COMPANY)
            .companyContentType(DEFAULT_COMPANY_CONTENT_TYPE)
            .company2(DEFAULT_COMPANY_2)
            .company2ContentType(DEFAULT_COMPANY_2_CONTENT_TYPE)
            .efficacy(DEFAULT_EFFICACY)
            .dose(DEFAULT_DOSE)
            .type(DEFAULT_TYPE)
            .storage(DEFAULT_STORAGE)
            .description(DEFAULT_DESCRIPTION);
        return vaccine;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Vaccine createUpdatedEntity(EntityManager em) {
        Vaccine vaccine = new Vaccine()
            .name(UPDATED_NAME)
            .company(UPDATED_COMPANY)
            .companyContentType(UPDATED_COMPANY_CONTENT_TYPE)
            .company2(UPDATED_COMPANY_2)
            .company2ContentType(UPDATED_COMPANY_2_CONTENT_TYPE)
            .efficacy(UPDATED_EFFICACY)
            .dose(UPDATED_DOSE)
            .type(UPDATED_TYPE)
            .storage(UPDATED_STORAGE)
            .description(UPDATED_DESCRIPTION);
        return vaccine;
    }

    @BeforeEach
    public void initTest() {
        vaccine = createEntity(em);
    }

    @Test
    @Transactional
    public void createVaccine() throws Exception {
        int databaseSizeBeforeCreate = vaccineRepository.findAll().size();
        // Create the Vaccine
        VaccineDTO vaccineDTO = vaccineMapper.toDto(vaccine);
        restVaccineMockMvc.perform(post("/api/vaccines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vaccineDTO)))
            .andExpect(status().isCreated());

        // Validate the Vaccine in the database
        List<Vaccine> vaccineList = vaccineRepository.findAll();
        assertThat(vaccineList).hasSize(databaseSizeBeforeCreate + 1);
        Vaccine testVaccine = vaccineList.get(vaccineList.size() - 1);
        assertThat(testVaccine.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testVaccine.getCompany()).isEqualTo(DEFAULT_COMPANY);
        assertThat(testVaccine.getCompanyContentType()).isEqualTo(DEFAULT_COMPANY_CONTENT_TYPE);
        assertThat(testVaccine.getCompany2()).isEqualTo(DEFAULT_COMPANY_2);
        assertThat(testVaccine.getCompany2ContentType()).isEqualTo(DEFAULT_COMPANY_2_CONTENT_TYPE);
        assertThat(testVaccine.getEfficacy()).isEqualTo(DEFAULT_EFFICACY);
        assertThat(testVaccine.getDose()).isEqualTo(DEFAULT_DOSE);
        assertThat(testVaccine.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testVaccine.getStorage()).isEqualTo(DEFAULT_STORAGE);
        assertThat(testVaccine.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createVaccineWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = vaccineRepository.findAll().size();

        // Create the Vaccine with an existing ID
        vaccine.setId(1L);
        VaccineDTO vaccineDTO = vaccineMapper.toDto(vaccine);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVaccineMockMvc.perform(post("/api/vaccines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vaccineDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Vaccine in the database
        List<Vaccine> vaccineList = vaccineRepository.findAll();
        assertThat(vaccineList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = vaccineRepository.findAll().size();
        // set the field null
        vaccine.setName(null);

        // Create the Vaccine, which fails.
        VaccineDTO vaccineDTO = vaccineMapper.toDto(vaccine);


        restVaccineMockMvc.perform(post("/api/vaccines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vaccineDTO)))
            .andExpect(status().isBadRequest());

        List<Vaccine> vaccineList = vaccineRepository.findAll();
        assertThat(vaccineList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllVaccines() throws Exception {
        // Initialize the database
        vaccineRepository.saveAndFlush(vaccine);

        // Get all the vaccineList
        restVaccineMockMvc.perform(get("/api/vaccines?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vaccine.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].companyContentType").value(hasItem(DEFAULT_COMPANY_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].company").value(hasItem(Base64Utils.encodeToString(DEFAULT_COMPANY))))
            .andExpect(jsonPath("$.[*].company2ContentType").value(hasItem(DEFAULT_COMPANY_2_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].company2").value(hasItem(Base64Utils.encodeToString(DEFAULT_COMPANY_2))))
            .andExpect(jsonPath("$.[*].efficacy").value(hasItem(DEFAULT_EFFICACY.intValue())))
            .andExpect(jsonPath("$.[*].dose").value(hasItem(DEFAULT_DOSE)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].storage").value(hasItem(DEFAULT_STORAGE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllVaccinesWithEagerRelationshipsIsEnabled() throws Exception {
        when(vaccineServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restVaccineMockMvc.perform(get("/api/vaccines?eagerload=true"))
            .andExpect(status().isOk());

        verify(vaccineServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllVaccinesWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(vaccineServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restVaccineMockMvc.perform(get("/api/vaccines?eagerload=true"))
            .andExpect(status().isOk());

        verify(vaccineServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getVaccine() throws Exception {
        // Initialize the database
        vaccineRepository.saveAndFlush(vaccine);

        // Get the vaccine
        restVaccineMockMvc.perform(get("/api/vaccines/{id}", vaccine.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(vaccine.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.companyContentType").value(DEFAULT_COMPANY_CONTENT_TYPE))
            .andExpect(jsonPath("$.company").value(Base64Utils.encodeToString(DEFAULT_COMPANY)))
            .andExpect(jsonPath("$.company2ContentType").value(DEFAULT_COMPANY_2_CONTENT_TYPE))
            .andExpect(jsonPath("$.company2").value(Base64Utils.encodeToString(DEFAULT_COMPANY_2)))
            .andExpect(jsonPath("$.efficacy").value(DEFAULT_EFFICACY.intValue()))
            .andExpect(jsonPath("$.dose").value(DEFAULT_DOSE))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.storage").value(DEFAULT_STORAGE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingVaccine() throws Exception {
        // Get the vaccine
        restVaccineMockMvc.perform(get("/api/vaccines/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVaccine() throws Exception {
        // Initialize the database
        vaccineRepository.saveAndFlush(vaccine);

        int databaseSizeBeforeUpdate = vaccineRepository.findAll().size();

        // Update the vaccine
        Vaccine updatedVaccine = vaccineRepository.findById(vaccine.getId()).get();
        // Disconnect from session so that the updates on updatedVaccine are not directly saved in db
        em.detach(updatedVaccine);
        updatedVaccine
            .name(UPDATED_NAME)
            .company(UPDATED_COMPANY)
            .companyContentType(UPDATED_COMPANY_CONTENT_TYPE)
            .company2(UPDATED_COMPANY_2)
            .company2ContentType(UPDATED_COMPANY_2_CONTENT_TYPE)
            .efficacy(UPDATED_EFFICACY)
            .dose(UPDATED_DOSE)
            .type(UPDATED_TYPE)
            .storage(UPDATED_STORAGE)
            .description(UPDATED_DESCRIPTION);
        VaccineDTO vaccineDTO = vaccineMapper.toDto(updatedVaccine);

        restVaccineMockMvc.perform(put("/api/vaccines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vaccineDTO)))
            .andExpect(status().isOk());

        // Validate the Vaccine in the database
        List<Vaccine> vaccineList = vaccineRepository.findAll();
        assertThat(vaccineList).hasSize(databaseSizeBeforeUpdate);
        Vaccine testVaccine = vaccineList.get(vaccineList.size() - 1);
        assertThat(testVaccine.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testVaccine.getCompany()).isEqualTo(UPDATED_COMPANY);
        assertThat(testVaccine.getCompanyContentType()).isEqualTo(UPDATED_COMPANY_CONTENT_TYPE);
        assertThat(testVaccine.getCompany2()).isEqualTo(UPDATED_COMPANY_2);
        assertThat(testVaccine.getCompany2ContentType()).isEqualTo(UPDATED_COMPANY_2_CONTENT_TYPE);
        assertThat(testVaccine.getEfficacy()).isEqualTo(UPDATED_EFFICACY);
        assertThat(testVaccine.getDose()).isEqualTo(UPDATED_DOSE);
        assertThat(testVaccine.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testVaccine.getStorage()).isEqualTo(UPDATED_STORAGE);
        assertThat(testVaccine.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingVaccine() throws Exception {
        int databaseSizeBeforeUpdate = vaccineRepository.findAll().size();

        // Create the Vaccine
        VaccineDTO vaccineDTO = vaccineMapper.toDto(vaccine);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVaccineMockMvc.perform(put("/api/vaccines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vaccineDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Vaccine in the database
        List<Vaccine> vaccineList = vaccineRepository.findAll();
        assertThat(vaccineList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteVaccine() throws Exception {
        // Initialize the database
        vaccineRepository.saveAndFlush(vaccine);

        int databaseSizeBeforeDelete = vaccineRepository.findAll().size();

        // Delete the vaccine
        restVaccineMockMvc.perform(delete("/api/vaccines/{id}", vaccine.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Vaccine> vaccineList = vaccineRepository.findAll();
        assertThat(vaccineList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
