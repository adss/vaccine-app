package com.covid19.vaccine.web.rest;

import com.covid19.vaccine.VaccineApp;
import com.covid19.vaccine.domain.VaccineType;
import com.covid19.vaccine.repository.VaccineTypeRepository;
import com.covid19.vaccine.service.VaccineTypeService;
import com.covid19.vaccine.service.dto.VaccineTypeDTO;
import com.covid19.vaccine.service.mapper.VaccineTypeMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link VaccineTypeResource} REST controller.
 */
@SpringBootTest(classes = VaccineApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class VaccineTypeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final byte[] DEFAULT_TYPE_IMAGE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_TYPE_IMAGE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_TYPE_IMAGE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_TYPE_IMAGE_CONTENT_TYPE = "image/png";

    @Autowired
    private VaccineTypeRepository vaccineTypeRepository;

    @Autowired
    private VaccineTypeMapper vaccineTypeMapper;

    @Autowired
    private VaccineTypeService vaccineTypeService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restVaccineTypeMockMvc;

    private VaccineType vaccineType;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VaccineType createEntity(EntityManager em) {
        VaccineType vaccineType = new VaccineType()
            .name(DEFAULT_NAME)
            .typeImage(DEFAULT_TYPE_IMAGE)
            .typeImageContentType(DEFAULT_TYPE_IMAGE_CONTENT_TYPE);
        return vaccineType;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VaccineType createUpdatedEntity(EntityManager em) {
        VaccineType vaccineType = new VaccineType()
            .name(UPDATED_NAME)
            .typeImage(UPDATED_TYPE_IMAGE)
            .typeImageContentType(UPDATED_TYPE_IMAGE_CONTENT_TYPE);
        return vaccineType;
    }

    @BeforeEach
    public void initTest() {
        vaccineType = createEntity(em);
    }

    @Test
    @Transactional
    public void createVaccineType() throws Exception {
        int databaseSizeBeforeCreate = vaccineTypeRepository.findAll().size();
        // Create the VaccineType
        VaccineTypeDTO vaccineTypeDTO = vaccineTypeMapper.toDto(vaccineType);
        restVaccineTypeMockMvc.perform(post("/api/vaccine-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vaccineTypeDTO)))
            .andExpect(status().isCreated());

        // Validate the VaccineType in the database
        List<VaccineType> vaccineTypeList = vaccineTypeRepository.findAll();
        assertThat(vaccineTypeList).hasSize(databaseSizeBeforeCreate + 1);
        VaccineType testVaccineType = vaccineTypeList.get(vaccineTypeList.size() - 1);
        assertThat(testVaccineType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testVaccineType.getTypeImage()).isEqualTo(DEFAULT_TYPE_IMAGE);
        assertThat(testVaccineType.getTypeImageContentType()).isEqualTo(DEFAULT_TYPE_IMAGE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createVaccineTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = vaccineTypeRepository.findAll().size();

        // Create the VaccineType with an existing ID
        vaccineType.setId(1L);
        VaccineTypeDTO vaccineTypeDTO = vaccineTypeMapper.toDto(vaccineType);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVaccineTypeMockMvc.perform(post("/api/vaccine-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vaccineTypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the VaccineType in the database
        List<VaccineType> vaccineTypeList = vaccineTypeRepository.findAll();
        assertThat(vaccineTypeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = vaccineTypeRepository.findAll().size();
        // set the field null
        vaccineType.setName(null);

        // Create the VaccineType, which fails.
        VaccineTypeDTO vaccineTypeDTO = vaccineTypeMapper.toDto(vaccineType);


        restVaccineTypeMockMvc.perform(post("/api/vaccine-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vaccineTypeDTO)))
            .andExpect(status().isBadRequest());

        List<VaccineType> vaccineTypeList = vaccineTypeRepository.findAll();
        assertThat(vaccineTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllVaccineTypes() throws Exception {
        // Initialize the database
        vaccineTypeRepository.saveAndFlush(vaccineType);

        // Get all the vaccineTypeList
        restVaccineTypeMockMvc.perform(get("/api/vaccine-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vaccineType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].typeImageContentType").value(hasItem(DEFAULT_TYPE_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].typeImage").value(hasItem(Base64Utils.encodeToString(DEFAULT_TYPE_IMAGE))));
    }
    
    @Test
    @Transactional
    public void getVaccineType() throws Exception {
        // Initialize the database
        vaccineTypeRepository.saveAndFlush(vaccineType);

        // Get the vaccineType
        restVaccineTypeMockMvc.perform(get("/api/vaccine-types/{id}", vaccineType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(vaccineType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.typeImageContentType").value(DEFAULT_TYPE_IMAGE_CONTENT_TYPE))
            .andExpect(jsonPath("$.typeImage").value(Base64Utils.encodeToString(DEFAULT_TYPE_IMAGE)));
    }
    @Test
    @Transactional
    public void getNonExistingVaccineType() throws Exception {
        // Get the vaccineType
        restVaccineTypeMockMvc.perform(get("/api/vaccine-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVaccineType() throws Exception {
        // Initialize the database
        vaccineTypeRepository.saveAndFlush(vaccineType);

        int databaseSizeBeforeUpdate = vaccineTypeRepository.findAll().size();

        // Update the vaccineType
        VaccineType updatedVaccineType = vaccineTypeRepository.findById(vaccineType.getId()).get();
        // Disconnect from session so that the updates on updatedVaccineType are not directly saved in db
        em.detach(updatedVaccineType);
        updatedVaccineType
            .name(UPDATED_NAME)
            .typeImage(UPDATED_TYPE_IMAGE)
            .typeImageContentType(UPDATED_TYPE_IMAGE_CONTENT_TYPE);
        VaccineTypeDTO vaccineTypeDTO = vaccineTypeMapper.toDto(updatedVaccineType);

        restVaccineTypeMockMvc.perform(put("/api/vaccine-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vaccineTypeDTO)))
            .andExpect(status().isOk());

        // Validate the VaccineType in the database
        List<VaccineType> vaccineTypeList = vaccineTypeRepository.findAll();
        assertThat(vaccineTypeList).hasSize(databaseSizeBeforeUpdate);
        VaccineType testVaccineType = vaccineTypeList.get(vaccineTypeList.size() - 1);
        assertThat(testVaccineType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testVaccineType.getTypeImage()).isEqualTo(UPDATED_TYPE_IMAGE);
        assertThat(testVaccineType.getTypeImageContentType()).isEqualTo(UPDATED_TYPE_IMAGE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingVaccineType() throws Exception {
        int databaseSizeBeforeUpdate = vaccineTypeRepository.findAll().size();

        // Create the VaccineType
        VaccineTypeDTO vaccineTypeDTO = vaccineTypeMapper.toDto(vaccineType);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVaccineTypeMockMvc.perform(put("/api/vaccine-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vaccineTypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the VaccineType in the database
        List<VaccineType> vaccineTypeList = vaccineTypeRepository.findAll();
        assertThat(vaccineTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteVaccineType() throws Exception {
        // Initialize the database
        vaccineTypeRepository.saveAndFlush(vaccineType);

        int databaseSizeBeforeDelete = vaccineTypeRepository.findAll().size();

        // Delete the vaccineType
        restVaccineTypeMockMvc.perform(delete("/api/vaccine-types/{id}", vaccineType.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<VaccineType> vaccineTypeList = vaccineTypeRepository.findAll();
        assertThat(vaccineTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
