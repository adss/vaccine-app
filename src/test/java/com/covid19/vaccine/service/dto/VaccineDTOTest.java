package com.covid19.vaccine.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.covid19.vaccine.web.rest.TestUtil;

public class VaccineDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(VaccineDTO.class);
        VaccineDTO vaccineDTO1 = new VaccineDTO();
        vaccineDTO1.setId(1L);
        VaccineDTO vaccineDTO2 = new VaccineDTO();
        assertThat(vaccineDTO1).isNotEqualTo(vaccineDTO2);
        vaccineDTO2.setId(vaccineDTO1.getId());
        assertThat(vaccineDTO1).isEqualTo(vaccineDTO2);
        vaccineDTO2.setId(2L);
        assertThat(vaccineDTO1).isNotEqualTo(vaccineDTO2);
        vaccineDTO1.setId(null);
        assertThat(vaccineDTO1).isNotEqualTo(vaccineDTO2);
    }
}
