package com.covid19.vaccine.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class VaccineMapperTest {

    private VaccineMapper vaccineMapper;

    @BeforeEach
    public void setUp() {
        vaccineMapper = new VaccineMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(vaccineMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(vaccineMapper.fromId(null)).isNull();
    }
}
