package com.covid19.vaccine.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class VaccineTypeMapperTest {

    private VaccineTypeMapper vaccineTypeMapper;

    @BeforeEach
    public void setUp() {
        vaccineTypeMapper = new VaccineTypeMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(vaccineTypeMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(vaccineTypeMapper.fromId(null)).isNull();
    }
}
